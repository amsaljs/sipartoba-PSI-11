package com.PSI.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="wisatavendor")
public class wisatavendor {
	
	@Id
	private Integer id_wisata;
	private String nama_vendor;
	private String nama_wisata;
	private String alamat;
	private Integer jumlah_komentar;
	private Integer tahun;
	private Integer id_akun;
	
	public Integer getId_wisata() {
		return id_wisata;
	}
	public void setId_wisata(Integer id_wisata) {
		this.id_wisata = id_wisata;
	}
	public String getNama_vendor() {
		return nama_vendor;
	}
	public void setNama_vendor(String nama_vendor) {
		this.nama_vendor = nama_vendor;
	}
	public String getNama_wisata() {
		return nama_wisata;
	}
	public void setNama_wisata(String nama_wisata) {
		this.nama_wisata = nama_wisata;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Integer getJumlah_komentar() {
		return jumlah_komentar;
	}
	public void setJumlah_komentar(Integer jumlah_komentar) {
		this.jumlah_komentar = jumlah_komentar;
	}
	public Integer getTahun() {
		return tahun;
	}
	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}
	public Integer getId_akun() {
		return id_akun;
	}
	public void setId_akun(Integer id_akun) {
		this.id_akun = id_akun;
	}
	
	
	

}
