package com.PSI.model;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.PSI.model.Akun;


@Entity
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Integer id_role;
	private String nama_role;
	private String ket;
	@OneToMany(mappedBy = "role")
    private Set<Akun> akun;
	
	/**
	 * @return the id_role
	 */
	public Integer getId_role() {
		return id_role;
	}
	/**
	 * @param id_role the id_role to set
	 */
	public void setId_role(Integer id_role) {
		this.id_role = id_role;
	}
	/**
	 * @return the role
	 */
	
	/**
	 * @return the ket
	 */
	public String getKet() {
		return ket;
	}
	/**
	 * @return the nama_role
	 */
	public String getNama_role() {
		return nama_role;
	}
	/**
	 * @param nama_role the nama_role to set
	 */
	public void setNama_role(String nama_role) {
		this.nama_role = nama_role;
	}
	/**
	 * @param ket the ket to set
	 */
	public void setKet(String ket) {
		this.ket = ket;
	}
	public Set<Akun> getAkun() {
		return akun;
	}
	public void setAkun(Set<Akun> akun) {
		this.akun = akun;
	}
	
	
	
}
