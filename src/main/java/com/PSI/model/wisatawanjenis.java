package com.PSI.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="wisatawanjenis")
public class wisatawanjenis {
	@Id
	private Integer id_wisatawan;
	private Integer lk;
	private Integer pr;
	public Integer getId_wisatawan() {
		return id_wisatawan;
	}
	public void setId_wisatawan(Integer id_wisatawan) {
		this.id_wisatawan = id_wisatawan;
	}
	public Integer getLk() {
		return lk;
	}
	public void setLk(Integer lk) {
		this.lk = lk;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	
	
}
