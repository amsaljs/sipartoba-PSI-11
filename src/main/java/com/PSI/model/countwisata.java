package com.PSI.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="countwisata")
public class countwisata {
	@Id
	private Integer id_wisata;
	private Integer tahun;
	private Integer accepted;
	private Integer rejected;
	private Integer disabled;
	
	
	public Integer getId_wisata() {
		return id_wisata;
	}
	public void setId_wisata(Integer id_wisata) {
		this.id_wisata = id_wisata;
	}
	public Integer getTahun() {
		return tahun;
	}
	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}
	public Integer getAccepted() {
		return accepted;
	}
	public void setAccepted(Integer accepted) {
		this.accepted = accepted;
	}
	public Integer getRejected() {
		return rejected;
	}
	public void setRejected(Integer rejected) {
		this.rejected = rejected;
	}
	public Integer getDisabled() {
		return disabled;
	}
	public void setDisabled(Integer disabled) {
		this.disabled = disabled;
	}

	
}
