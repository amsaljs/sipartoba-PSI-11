package com.PSI.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Fasilitas {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_fasilitas;
	private String nama_fasilitas;
	private String biaya;
	private String keterangan;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "t_wisata_id")
	private TempatWisata wisata;

	public int getId_fasilitas() {
		return id_fasilitas;
	}

	public void setId_fasilitas(int id_fasilitas) {
		this.id_fasilitas = id_fasilitas;
	}

	public String getNama_fasilitas() {
		return nama_fasilitas;
	}

	public void setNama_fasilitas(String nama_fasilitas) {
		this.nama_fasilitas = nama_fasilitas;
	}

	public String getBiaya() {
		return biaya;
	}

	public void setBiaya(String biaya) {
		this.biaya = biaya;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public TempatWisata getWisata() {
		return wisata;
	}

	public void setWisata(TempatWisata wisata) {
		this.wisata = wisata;
	}

	
}
