package com.PSI.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hotwisata")
public class hotwisata {
	@Id
	private Integer id_wisata;
	private String nama_wisata;
	private Integer tahun;
	private Integer jumlah_review;
	private String alamat;
	private String gambar;
	
		
	public String getGambar() {
		return gambar;
	}
	public void setGambar(String gambar) {
		this.gambar = gambar;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Integer getId_wisata() {
		return id_wisata;
	}
	public void setId_wisata(Integer id_wisata) {
		this.id_wisata = id_wisata;
	}
	public String getNama_wisata() {
		return nama_wisata;
	}
	public void setNama_wisata(String nama_wisata) {
		this.nama_wisata = nama_wisata;
	}
	public Integer getTahun() {
		return tahun;
	}
	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}
	public Integer getJumlah_review() {
		return jumlah_review;
	}
	public void setJumlah_review(Integer jumlah_review) {
		this.jumlah_review = jumlah_review;
	}
	
	
}
