package com.PSI.model;


import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Kategori {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_kategori;
	private String nama_kat;
	
	@OneToMany(mappedBy = "kategori")
    private Set<TempatWisata> tempatwisata;
	
	public int getId_kategori() {
		return id_kategori;
	}
	public void setId_kategori(int id_kategori) {
		this.id_kategori = id_kategori;
	}
	public String getNama_kat() {
		return nama_kat;
	}
	public void setNama_kat(String nama_kat) {
		this.nama_kat = nama_kat;
	}
	public Set<TempatWisata> getTempatwisata() {
		return tempatwisata;
	}
	public void setTempatwisata(Set<TempatWisata> tempatwisata) {
		this.tempatwisata = tempatwisata;
	}
	
	
	
}
