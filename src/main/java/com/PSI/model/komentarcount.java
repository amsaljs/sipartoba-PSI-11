package com.PSI.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="komentarcount")
public class komentarcount {
	@Id
	private Integer jumlah;
	private Integer wisata_id;
	private String nama;
	private Integer tahun;
	
	
	
	public Integer getTahun() {
		return tahun;
	}
	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public Integer getJumlah() {
		return jumlah;
	}
	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}
	public Integer getWisata_id() {
		return wisata_id;
	}
	public void setWisata_id(Integer wisata_id) {
		this.wisata_id = wisata_id;
	}
	
	
}
