package com.PSI.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="datawisatawan")
public class datawisatawan {
	
	@Id
	private Integer id_wisatawan;
	private Integer lk;
	private Integer pr;
	private Integer dalam;
	private Integer luar;
	private Integer tahun;
	
	
	public Integer getTahun() {
		return tahun;
	}
	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}
	public Integer getId_wisatawan() {
		return id_wisatawan;
	}
	public void setId_wisatawan(Integer id_wisatawan) {
		this.id_wisatawan = id_wisatawan;
	}
	public Integer getLk() {
		return lk;
	}
	public void setLk(Integer lk) {
		this.lk = lk;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public Integer getDalam() {
		return dalam;
	}
	public void setDalam(Integer dalam) {
		this.dalam = dalam;
	}
	public Integer getLuar() {
		return luar;
	}
	public void setLuar(Integer luar) {
		this.luar = luar;
	}
	
	
}
