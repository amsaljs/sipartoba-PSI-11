package com.PSI.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Wisatawan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_wisatawan;
	private String nama_wisatawan;
	private String telp;
	private String alamat;
	private String country;
	private String email;
	private String gender;
	private String tgl_create;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "t_akun_id")
	private Akun akun;
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getId_wisatawan() {
		return id_wisatawan;
	}
	public void setId_wisatawan(Integer id_wisatawan) {
		this.id_wisatawan = id_wisatawan;
	}
	public String getNama_wisatawan() {
		return nama_wisatawan;
	}
	public void setNama_wisatawan(String nama_wisatawan) {
		this.nama_wisatawan = nama_wisatawan;
	}
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Akun getAkun() {
		return akun;
	}
	public void setAkun(Akun akun) {
		this.akun = akun;
	}

	public String getTgl_create() {
		return tgl_create;
	}
	public void setTgl_create(String tgl_create) {
		this.tgl_create = tgl_create;
	}
	
	
	
}
