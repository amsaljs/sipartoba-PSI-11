package com.PSI.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="viewgrafikwisata")
public class viewgrafikwisata {
	@Id
	@Column(name="id_wisata")
	private Integer id;
	private Integer accept;
	private Integer reject;
	private Integer request;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAccept() {
		return accept;
	}
	public void setAccept(Integer accept) {
		this.accept = accept;
	}
	public Integer getReject() {
		return reject;
	}
	public void setReject(Integer reject) {
		this.reject = reject;
	}
	public Integer getRequest() {
		return request;
	}
	public void setRequest(Integer request) {
		this.request = request;
	}
	
	
}
