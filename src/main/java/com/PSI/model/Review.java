package com.PSI.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Review {
	@Id
    @GeneratedValue (strategy = GenerationType.AUTO)

    private Integer id_review;
    private String komentar;
    private Integer rating;
    @Column(name="nama_wisata")
    private String nama;
    @Column(name="nama_reviewer")
    private String nama_reviewer;
    private String tgl_review;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "t_wisata_id")
	private TempatWisata wisata;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "t_reviewer_id")
	private Akun akun;
	
	
	public TempatWisata getWisata() {
		return wisata;
	}
	public void setWisata(TempatWisata wisata) {
		this.wisata = wisata;
	}
	public Akun getAkun() {
		return akun;
	}
	public void setAkun(Akun akun) {
		this.akun = akun;
	}
	public Integer getId_review() {
		return id_review;
	}
	public void setId_review(Integer id_review) {
		this.id_review = id_review;
	}
	public String getKomentar() {
		return komentar;
	}
	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public String getNama_reviewer() {
		return nama_reviewer;
	}
	public void setNama_reviewer(String nama_reviewer) {
		this.nama_reviewer = nama_reviewer;
	}
	public String getTgl_review() {
		return tgl_review;
	}
	public void setTgl_review(String tgl_review) {
		this.tgl_review = tgl_review;
	}
	
	  
}