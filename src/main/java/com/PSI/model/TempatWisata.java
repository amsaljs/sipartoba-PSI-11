package com.PSI.model;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;



@Entity

public class TempatWisata {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    
    @Column(name="id_wisata")
    private Integer id;

    @Column (name ="nama_wisata")
    private String nama;
    private String alamat;
    private String telp;
    private String nama_vendor;
    private String tgl_request;
    private String gambar;
    private String tgl_konfirmasi;
    private String nama_kategori;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_kategori")
	private Kategori kategori;
    
	@OneToMany(mappedBy = "wisata")
    private Set<Fasilitas> fasilitas;
    
	@OneToMany(mappedBy = "wisata")
    private Set<Review> review;
	
	
    private String status;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "t_id_vendor")
	private Vendor vendor;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "t_akun_id")
	private Akun akun;
    
    @Version
    @Column(name = "optVersion", columnDefinition ="integer DEFAULT 0")
    private Integer version;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
       	
    public Set<Review> getReview() {
		return review;
	}
	public void setReview(Set<Review> review) {
		this.review = review;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}
	
	public String getGambar() {
		return gambar;
	}
	public void setGambar(String gambar) {
		this.gambar = gambar;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getNama_vendor() {
		return nama_vendor;
	}
	public void setNama_vendor(String nama_vendor) {
		this.nama_vendor = nama_vendor;
	}
	public Kategori getKategori() {
		return kategori;
	}
	
	public String getTgl_request() {
		return tgl_request;
	}
	public void setTgl_request(String tgl_request) {
		this.tgl_request = tgl_request;
	}
	public String getTgl_konfirmasi() {
		return tgl_konfirmasi;
	}
	public void setTgl_konfirmasi(String tgl_konfirmasi) {
		this.tgl_konfirmasi = tgl_konfirmasi;
	}
	public void setKategori(Kategori kategori) {
		this.kategori = kategori;
	}
	public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	public String getNama_kategori() {
		return nama_kategori;
	}
	public void setNama_kategori(String nama_kategori) {
		this.nama_kategori = nama_kategori;
	}
	public Akun getAkun() {
		return akun;
	}
	public void setAkun(Akun akun) {
		this.akun = akun;
	}
	public Set<Fasilitas> getFasilitas() {
		return fasilitas;
	}
	public void setFasilitas(Set<Fasilitas> fasilitas) {
		this.fasilitas = fasilitas;
	}

}
