package com.PSI.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Vendor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Integer id_vendor;
	private String nama_vendor;
	private String telp;
	private String email;
	private String alamat;

	
	@OneToMany(mappedBy = "vendor")
    private Set<TempatWisata> tempatWisata;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "t_akun_id")
	private Akun akun;

	
	public Integer getId_vendor() {
		return id_vendor;
	}
	public void setId_vendor(Integer id_vendor) {
		this.id_vendor = id_vendor;
	}
	
	public String getNama_vendor() {
		return nama_vendor;
	}
	public void setNama_vendor(String nama_vendor) {
		this.nama_vendor = nama_vendor;
	}
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Set<TempatWisata> getTempatWisata() {
		return tempatWisata;
	}
	public void setTempatWisata(Set<TempatWisata> tempatWisata) {
		this.tempatWisata = tempatWisata;
	}
	public Akun getAkun() {
		return akun;
	}
	public void setAkun(Akun akun) {
		this.akun = akun;
	}
	
	
	
}
