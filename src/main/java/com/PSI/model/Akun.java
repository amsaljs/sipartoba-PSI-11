package com.PSI.model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
//import javax.persistence.Transient;
import javax.persistence.OneToOne;


@Entity
public class Akun {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nama_akun;
	private String username;
	private String password;
	private String role_name;
	private String date_create;
//	private boolean enabled;
	
	@OneToMany(mappedBy = "akun")
    private Set<Review> review;
	
	
	@OneToMany(mappedBy="akun")
	private Set<TempatWisata> tempatwisata;
	
	@OneToOne(mappedBy = "akun",cascade = CascadeType.ALL)
	private Wisatawan wisatawan;
	
	@OneToOne(mappedBy = "akun",cascade = CascadeType.ALL)
	private Vendor vendor;

//	@OneToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "t_vendor_id")
//	private Vendor vendor;
	
	
//	@OneToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "t_dinas_id")
//	private Dinas dinas;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "t_id_role")
	private Role role;


	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNama_akun() {
		return nama_akun;
	}
	
	public void setNama_akun(String nama_akun) {
		this.nama_akun = nama_akun;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public Set<TempatWisata> getTempatwisata() {
		return tempatwisata;
	}

	public void setTempatwisata(Set<TempatWisata> tempatwisata) {
		this.tempatwisata = tempatwisata;
	}



	public Wisatawan getWisatawan() {
		return wisatawan;
	}

	public void setWisatawan(Wisatawan wisatawan) {
		this.wisatawan = wisatawan;
	}

	public String getDate_create() {
		return date_create;
	}

	public void setDate_create(String date_create) {
		this.date_create = date_create;
	}


	public Set<Review> getReview() {
		return review;
	}

	public void setReview(Set<Review> review) {
		this.review = review;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	
	
		
}