package com.PSI.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Dinas {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Integer id_dinas;
	private String email;
	private String telp;
	private String alamat;
	private String nama_dinas;
	@Column(name = "confirmation_token")
	private String confirmationToken;
	private boolean enable;
	
//	@OneToOne(mappedBy = "dinas",cascade = CascadeType.ALL)
//	private Akun akun;

	
	public Integer getId_dinas() {
		return id_dinas;
	}
	public void setId_dinas(Integer id_dinas) {
		this.id_dinas = id_dinas;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}
	
	public String getNama_dinas() {
		return nama_dinas;
	}
	public void setNama_dinas(String nama_dinas) {
		this.nama_dinas = nama_dinas;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getConfirmationToken() {
		return confirmationToken;
	}
	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
//	public Akun getAkun() {
//		return akun;
//	}
//	public void setAkun(Akun akun) {
//		this.akun = akun;
//	}
	
	
	
}
