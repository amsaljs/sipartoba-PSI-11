package com.PSI.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.PSI.model.Vendor;

@Repository
public interface VendorRepo {
	List<Vendor> getAllVendor();
	Vendor saveOrUpdateVendor(Vendor vendor);
	Vendor getIdVendor(String nama_vendor);
	
}
