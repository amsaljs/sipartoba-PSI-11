package com.PSI.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Dinas;

@Repository("dinasRepository")
public interface dRepo extends CrudRepository<Dinas, Long>{
	 Dinas findByEmail(String email);
	 Dinas findByConfirmationToken(String confirmationToken);
}
