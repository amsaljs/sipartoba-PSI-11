package com.PSI.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.PSI.model.Fasilitas;

@Repository
public interface FasilitasRepo {
	List<Fasilitas> getAllFalisitas();
	Fasilitas SaveOrUpdate(Fasilitas fasilitas);
	List<Fasilitas> getAllFasilitasByIdWisata(Integer id);
	
}
