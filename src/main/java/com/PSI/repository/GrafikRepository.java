package com.PSI.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.PSI.model.countwisata;
import com.PSI.model.datawisatawan;
import com.PSI.model.hotwisata;
import com.PSI.model.viewgrafikwisata;
import com.PSI.model.wisatavendor;
import com.PSI.model.wisatawanjenis;
import com.PSI.model.komentarcount;
import com.PSI.model.recommendedwisata;;

@Repository
public interface GrafikRepository {
	List<countwisata> listWisata();
	List<viewgrafikwisata> listGrafik();
	List<komentarcount> listKomentar();
	List<komentarcount> getDataByIdWisata(int id);
	List<recommendedwisata> getAllData();
	List<hotwisata> getAllDataHotWisata();
	wisatavendor getAllWisataVendorByIdAkun(Integer id);
	List<datawisatawan> getAllDataWisatawan();
	List<wisatawanjenis> getAllDataJenis();
	
	
}
