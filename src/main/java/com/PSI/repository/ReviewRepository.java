package com.PSI.repository;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.PSI.model.Review;

@Repository
public interface ReviewRepository {
	int rating(int id);
	Review findRatingbyId(int id);
	Review SaveOrUpdate(Review review);
	List<Review> getAllData();
	List<Review> getAllDataByNama(String nama_wisata);
	void hapus (Integer id_review);
}
