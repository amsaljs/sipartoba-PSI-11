package com.PSI.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.PSI.model.Role;

@Repository
public interface RoleRepo {
	Role getIdRole(String nama_role);
	List <Role> getAllRole ();
}
