package com.PSI.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Review;

@Repository("reviewRepository")
public interface reviewRepo extends CrudRepository<Review,Long> {
	Review findByNama(String nama);
	Iterable<Review> findAllByNama(String nama_wisat);
}
