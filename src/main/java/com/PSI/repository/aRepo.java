package com.PSI.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Akun;


@Repository("aRepository")
public interface aRepo extends CrudRepository<Akun, Long>{
	Akun findByUsername(String username);
	List<Akun> findAll();
}
