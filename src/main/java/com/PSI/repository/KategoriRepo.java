package com.PSI.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.PSI.model.Kategori;

@Repository
public interface KategoriRepo {
	Kategori getIdKategori(String nama_kat);
	List <Kategori> getAllKategori ();
}
