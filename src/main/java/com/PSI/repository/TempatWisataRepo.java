package com.PSI.repository;

import java.util.List;


import org.springframework.stereotype.Repository;

import com.PSI.model.TempatWisata;

@Repository
public interface TempatWisataRepo{
	List <TempatWisata> listWisata ();
    TempatWisata saveorUpdate (TempatWisata wisata);
    TempatWisata getIdWisata(Integer id_wisata);
    List<TempatWisata> getWisataById(Integer id_wisata);
    // ++++++++++++++ fungsi get wisata by status dan by kategori
    List<TempatWisata> getWisataByStatusAndKategori(String kategori, String status);
    
    void hapus (Integer id_wisata);
    List<TempatWisata> getAllTambahWisataByIdVendor(int id_vendor);
	List<TempatWisata> getAllTambahWisataByStatus(String status);
	List<TempatWisata> getWisataByKategori(String kategori);
	void updateStatusTambahWisataById(int id_wisata);
	void updateStatusTambahWisataIgnoreById(int id_wisata);
	void updateStatusTambahWisataDisableById(int id_wisata);
	TempatWisata findWisataById(int id_wisata);
}

