package com.PSI.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Vendor;
@Repository("vendorRepository")

public interface vRepo extends CrudRepository<Vendor, Long>{
	 Vendor findByEmail(String email);
	 Vendor findByAkun(Integer id);
}
