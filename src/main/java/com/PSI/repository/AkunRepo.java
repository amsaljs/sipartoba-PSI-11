package com.PSI.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.PSI.model.Akun;

@Repository
public interface AkunRepo {
	List<Akun> getAllAkun();
	Akun saveOrUpdate(Akun akun);
	Akun login(String username, String password);
	Akun saveOrUpdateWisatawan(Akun akun);
	Akun saveOrUpdateDinas(Akun akun);

}
