package com.PSI.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.Wisatawan;

@Repository("wRepo")
public interface wRepo extends CrudRepository<Wisatawan, Long>{
	 Wisatawan findByEmail(String email);
}
