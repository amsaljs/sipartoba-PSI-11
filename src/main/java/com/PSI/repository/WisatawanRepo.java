package com.PSI.repository;

import java.util.List;
import com.PSI.model.Wisatawan;

public interface WisatawanRepo {
	List<Wisatawan> getAllWisatawan();
	void save(Wisatawan wisatawan);
	Wisatawan saveOrUpdateWisatawan(Wisatawan wisatawan);
	
}
