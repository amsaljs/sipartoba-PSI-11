package com.PSI.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.PSI.model.TempatWisata;
//import com.PSI.model.Vendor;


@Repository("tempatwisataRepository")
public interface tRepo extends CrudRepository<TempatWisata, Long>{
	TempatWisata findById(Integer id);
	TempatWisata findByNama(String nama);
}
