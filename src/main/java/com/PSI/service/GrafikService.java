package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.countwisata;
import com.PSI.model.datawisatawan;
import com.PSI.model.hotwisata;
import com.PSI.model.komentarcount;
import com.PSI.model.recommendedwisata;
import com.PSI.model.viewgrafikwisata;
import com.PSI.model.wisatavendor;
import com.PSI.model.wisatawanjenis;
import com.PSI.repository.GrafikRepository;

@Service
public class GrafikService implements GrafikRepository{

	private EntityManagerFactory entityManagerFactory;
	
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}
	
	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public List<countwisata> listWisata() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<countwisata> count = em.createQuery("from countwisata", countwisata.class).getResultList();
		em.close();
		return count;
	}

	@Override
	public List<viewgrafikwisata> listGrafik() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<viewgrafikwisata> data = em.createQuery("from viewgrafikwisata", viewgrafikwisata.class).getResultList();
		em.close();
		return data;
	}

	@Override
	public List<komentarcount> listKomentar() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<komentarcount> data = em.createQuery("from komentarcount", komentarcount.class).getResultList();
		em.close();
		return data;
	
	}

	@Override
	public List<komentarcount> getDataByIdWisata(int id) {
		EntityManager em = entityManagerFactory.createEntityManager();
        List<komentarcount> count = null;
        try {
			count = em.createQuery("from komentarcount where wisata_id='"+id+"'", komentarcount.class).getResultList();
		} catch (Exception e) {
				count = null;
		}
        em.close();
        return count;
	}

	@Override
	public List<recommendedwisata> getAllData() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<recommendedwisata>	recommend = em.createQuery("from recommendedwisata", recommendedwisata.class).getResultList();
        em.close();
        return recommend;
	}

	@Override
	public List<hotwisata> getAllDataHotWisata() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<hotwisata>	hot = em.createQuery("from hotwisata", hotwisata.class).getResultList();
        em.close();
        return hot;
	}

	@Override
	public wisatavendor getAllWisataVendorByIdAkun(Integer id) {
		EntityManager em = entityManagerFactory.createEntityManager();
		wisatavendor data = null;
		try {
			data = em.createQuery("from wisatavendor where id_akun='"+id+"'", wisatavendor.class).getSingleResult();
		} catch (Exception e) {
			data= null;
		}
				
        em.close();
        return data;
	}

	@Override
	public List<datawisatawan> getAllDataWisatawan() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<datawisatawan>	data = em.createQuery("from datawisatawan", datawisatawan.class).getResultList();
        em.close();
        return data;
	}

	@Override
	public List<wisatawanjenis> getAllDataJenis() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<wisatawanjenis>	data = em.createQuery("from wisatawanjenis", wisatawanjenis.class).getResultList();
        em.close();
        return data;
	}
	
	
	
		
}
