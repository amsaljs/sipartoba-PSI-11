package com.PSI.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.Wisatawan;
import com.PSI.repository.wRepo;

@Service("wService")
public class wService {
	private wRepo WisatawanRepository;


	@Autowired
	public wService(wRepo WisatawanRepository) {
		this.WisatawanRepository = WisatawanRepository;
	}
	
	public Wisatawan findByEmail(String email) {
		return WisatawanRepository.findByEmail(email);
	}
	
	
	public void saveWisatawan(Wisatawan wisatawan) {
		WisatawanRepository.save(wisatawan);
	}
}
