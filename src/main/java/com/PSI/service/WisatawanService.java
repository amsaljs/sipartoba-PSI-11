package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.repository.WisatawanRepo;
import com.PSI.model.Wisatawan;

@Service
public class WisatawanService implements WisatawanRepo{
	
	@Autowired
	private WisatawanRepo wr;
	
	@Override
	public void save(Wisatawan wisatawan) {
		wr.save(wisatawan);
	}
	
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public List<Wisatawan> getAllWisatawan() {
		EntityManager em = entityManagerFactory.createEntityManager();
		return em.createQuery("from Wisatawan", Wisatawan.class).getResultList();
	}


	@Override
	public Wisatawan saveOrUpdateWisatawan(Wisatawan wisatawan) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Wisatawan saved = em.merge(wisatawan);
		em.getTransaction().commit();
		return saved;
	}
}
