package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.PSI.model.Fasilitas;
import com.PSI.repository.FasilitasRepo;

@Service
public class FasiltasService implements FasilitasRepo{
	private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;  
    }



	@Override
	public List<Fasilitas> getAllFalisitas() {
		EntityManager em = emf.createEntityManager();
		List<Fasilitas> fat= null; 
        try {
			fat = em.createQuery("from Fasilitas", Fasilitas.class).getResultList();
		} catch (Exception e) {
				fat = null;
		}
        em.close();
		return fat;
	}
	
	//save
	@Override
	public Fasilitas SaveOrUpdate(Fasilitas fasilitas) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Fasilitas saved = em.merge(fasilitas);
		em.getTransaction().commit();
		em.close();
		return saved;
	}
    
	
	/* (non-Javadoc)
	 * @see com.PSI.repository.FasilitasRepo#getAllFasilitasByIdWisata(java.lang.Integer)
	 */
	@Override
	public List<Fasilitas> getAllFasilitasByIdWisata(Integer id) {
		EntityManager em = emf.createEntityManager();
		List<Fasilitas> fat= null; 
        try {
			fat = em.createQuery("from Fasilitas where t_wisata_id='"+id+"'", Fasilitas.class).getResultList();
		} catch (Exception e) {
				fat = null;
		}
        em.close();
		return fat;		
	}



	public EntityManagerFactory getEmf() {
		return emf;
	}  
    
}

