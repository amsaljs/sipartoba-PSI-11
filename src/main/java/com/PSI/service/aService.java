package com.PSI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.Akun;
import com.PSI.repository.RoleRepo;
import com.PSI.repository.aRepo;


@Service("aService")
public class aService {
	private aRepo aRepository;
	@Autowired
	private RoleRepo rRepo;
	
	@Autowired
	public aService(aRepo aRepository) {
		this.aRepository = aRepository;
	}
	
	public Akun findByUsername(String username) {
		return aRepository.findByUsername(username);
	}
	
	public void saveAkun(Akun akun) {
		akun.setRole(rRepo.getIdRole("Vendor"));
		akun.setRole_name("Vendor");
		aRepository.save(akun);
	}
	
	public void saveAkunWisatawan(Akun akun) {
		akun.setRole(rRepo.getIdRole("Wisatawan"));
		akun.setRole_name("Wisatawan");
		aRepository.save(akun);
	}
	
	public List<Akun> findAllAkun(){
		return aRepository.findAll();
	}
	
}
