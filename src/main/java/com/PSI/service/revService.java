package com.PSI.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.Review;
import com.PSI.repository.reviewRepo;

@Service("revService")
public class revService {
	
	private reviewRepo reviewRepository;

	@Autowired
	public revService(reviewRepo reviewRepository) {
		this.reviewRepository = reviewRepository;
	}
	
	public Review findByNama(String nama) {
		return reviewRepository.findByNama(nama);
	}
	
	public Iterable<Review> findAllByNama(String nama){
		return reviewRepository.findAllByNama(nama);
	}
	
	
	
}
