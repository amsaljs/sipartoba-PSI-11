package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.Akun;
//import com.PSI.model.TempatWisata;
import com.PSI.repository.AkunRepo;
import com.PSI.repository.RoleRepo;

@Service
@Transactional
public class AkunService implements AkunRepo{

	private PasswordService passwords;
	@Autowired
	private RoleRepo rRepo;
	private EntityManagerFactory entityManagerFactory;
	
	public PasswordService getPassword() {
		return passwords;
	}
	@Autowired
	public void setPassword(PasswordService password) {
		this.passwords = password;
	}
	
	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
    
	
    @Override
	public List<Akun> getAllAkun() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<Akun> akun = null;
		try {
			akun = em.createQuery("from Akun", Akun.class).getResultList();
		} catch (Exception e) {
			akun= null;
		}
		em.close();
		return akun;
	}
    
        
    @Override
	public Akun login(String username, String password) {
		List<Akun> users = getAllAkun();
		for(int i=0; i<users.size(); i++){
			if(users.get(i).getUsername().equals(username)){
				if(passwords.matches(password, users.get(i).getPassword())) return users.get(i);
				else return null;
			}
		}
		return null;
	}
    
    @Override
	public Akun saveOrUpdate(Akun akun) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		akun.setRole(rRepo.getIdRole("Vendor"));
		akun.setRole_name("Vendor");
		Akun saved = em.merge(akun);
		em.getTransaction().commit();
		em.close();
		return saved;
	}
    
    @Override
    public Akun saveOrUpdateWisatawan(Akun akun) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		akun.setRole(rRepo.getIdRole("Wisatawan"));
		akun.setRole_name("Wisatawan");
		Akun saved = em.merge(akun);
		em.getTransaction().commit();
		em.close();
		return saved;
	}
    
    @Override
    public Akun saveOrUpdateDinas(Akun akun) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		akun.setRole(rRepo.getIdRole("Dinas Pariwisata"));
		akun.setRole_name("Dinas Pariwisata");
		Akun saved = em.merge(akun);
		em.getTransaction().commit();
		em.close();
		return saved;
	}

}