package com.PSI.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.PSI.model.Vendor;
import com.PSI.repository.vRepo;

@Service("vService")
public class vService {
	private vRepo vendorRepository;
	

	@Autowired
	public vService(vRepo vendorRepository) {
		this.vendorRepository = vendorRepository;
	}
	
	public Vendor findByEmail(String email) {
		return vendorRepository.findByEmail(email);
	}
		
	public Vendor findByAkun(Integer id) {
		return vendorRepository.findByAkun(id);
	}
	
	public void saveVendor(Vendor vendor) {
		vendorRepository.save(vendor);
	}
	

}
