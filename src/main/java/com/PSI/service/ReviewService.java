package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.Review;
import com.PSI.repository.ReviewRepository;

@Service
@Transactional
public class ReviewService implements ReviewRepository{
	
	private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;  
    }

    @Override
	public Review findRatingbyId(int id) {
    	EntityManager em = emf.createEntityManager();
    	return em.find(Review.class,id);
	}
    
    
    
	@Override
	public List<Review> getAllData() {
		EntityManager em = emf.createEntityManager();
		List<Review> review = null;
		try {
			review = em.createQuery("from Review", Review.class).getResultList();
		} catch (Exception e) {
			review = null;
		}
		em.close();
		return review;
	}
	
	

	@Override
	public List<Review> getAllDataByNama(String nama_wisata) {
		EntityManager em = emf.createEntityManager();
		List<Review> review = null;
		try {
			review = em.createQuery("from Review where nama="+nama_wisata+"'", Review.class).getResultList();
		} catch (Exception e) {
			review = null;
		}
		em.close();
		return review;
	}
	
	

	@Override
	public Review SaveOrUpdate(Review review) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Review saved = em.merge(review);
		em.getTransaction().commit();
		em.close();
		return saved;
	}

	public int rating(int id) {
    	Review review = findRatingbyId(id);
    	review.setRating(review.getRating()+1);
    	review = SaveOrUpdate(review);
    	return review.getRating();
    }
    

    
	public EntityManagerFactory getEmf() {
		return emf;
	}

	@Override
	public void hapus(Integer id_review) {
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Review.class, id_review));
        em.getTransaction().commit(); 
        em.close();
		
	}
	
	
    
    
}
