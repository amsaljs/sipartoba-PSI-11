package com.PSI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.Dinas;
import com.PSI.repository.dRepo;

@Service("dService")
public class dService {
	private dRepo dinasRepository;
	

	@Autowired
	public dService(dRepo dinasRepository) {
		this.dinasRepository = dinasRepository;
	}
	
	public Dinas findByEmail(String email) {
		return dinasRepository.findByEmail(email);
	}
	
	public Dinas findByConfirmationToken(String confirmationToken) {
		return dinasRepository.findByConfirmationToken(confirmationToken);
	}
	
	public void saveDinas(Dinas dinas) {
		dinasRepository.save(dinas);
	}
}
