package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.PSI.model.Kategori;
import com.PSI.repository.KategoriRepo;

@Service
public class KategoriService implements KategoriRepo {
	private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;  
    }
    
    
    @Override
	public List<Kategori> getAllKategori() {
		EntityManager em = emf.createEntityManager();
		List<Kategori> kat = null;
		try {
			kat = em.createQuery("from Kategori", Kategori.class).getResultList();
		} catch (Exception e) {
			kat = null;
		}
		em.close();
		return kat;
	}
    
    @Override
    public Kategori getIdKategori(String nama_kat) {
        EntityManager em=emf.createEntityManager();
        Kategori kat = null;
        try {
			kat = em.createQuery("from Kategori where nama_kat='"+nama_kat+"'", Kategori.class).getSingleResult();
		} catch (Exception e) {
			kat = null;
		}
        em.close();
        return kat;
    }
        
}	
