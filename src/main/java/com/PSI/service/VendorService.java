package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.PSI.repository.VendorRepo;
import com.PSI.model.Vendor;

@Service
public class VendorService implements VendorRepo{


	private EntityManagerFactory entityManagerFactory;

	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
		
	
	@Override
	public List<Vendor> getAllVendor() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<Vendor> vendor =null;
		try {
			vendor = em.createQuery("from Vendor", Vendor.class).getResultList();
		} catch (Exception e) {
			vendor = null;
		}
		em.close();
		return vendor;
	}


	@Override
	public Vendor saveOrUpdateVendor(Vendor vendor) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Vendor saved = em.merge(vendor);
		em.getTransaction().commit();
		em.close();
		return saved;
	}
	
	@Override
    public Vendor getIdVendor(String nama_vendor) {
        EntityManager em=entityManagerFactory.createEntityManager();
        Vendor vendor = null;
        try {
			vendor = em.createQuery("from Vendor where nama_vendor='"+nama_vendor+"'", Vendor.class).getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
        em.close();
        return vendor;
    }
}
