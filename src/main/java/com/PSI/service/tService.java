package com.PSI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PSI.model.TempatWisata;
import com.PSI.repository.tRepo;


@Service("tempatwisataService")
public class tService {
	private tRepo tempatwisataRepository;
	
	@Autowired
	public tService(tRepo tempatwisataRepository) {
		this.tempatwisataRepository = tempatwisataRepository;
	}
	
	public TempatWisata findByIdWisata(Integer id) {
		return tempatwisataRepository.findById(id);
	}
	
	public TempatWisata findByNama(String nama) {
		return tempatwisataRepository.findByNama(nama);
	}
}
