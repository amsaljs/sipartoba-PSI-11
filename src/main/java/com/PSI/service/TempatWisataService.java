package com.PSI.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.PSI.model.TempatWisata;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.repository.KategoriRepo;


@Service
@Transactional
public class TempatWisataService implements TempatWisataRepo{
	

	@Autowired
	private KategoriRepo kRepo;
	private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;  
    }

    @Override
    public List<TempatWisata> listWisata() {
        EntityManager em = emf.createEntityManager();
        List<TempatWisata> wisata = null;
        try {
			wisata = em.createQuery("from TempatWisata", TempatWisata.class).getResultList();
		} catch (Exception e) {
				wisata = null;
		}
        em.close();
        return wisata;
    }
    
    @Override
    public List<TempatWisata> getWisataById(Integer id_wisata) {
        EntityManager em = emf.createEntityManager();
        List<TempatWisata> wisata = null;
        try {
			wisata = em.createQuery("from TempatWisata where id_wisata='"+id_wisata+"'", TempatWisata.class).getResultList();
		} catch (Exception e) {
				wisata = null;
		}
        em.close();
        return wisata;   
    }
    
    @Override
    public List<TempatWisata> getWisataByKategori(String kategori) {
        EntityManager em = emf.createEntityManager();
        List<TempatWisata> wisata = null;
        try {
			wisata = em.createQuery("from TempatWisata where nama_kategori='"+kategori+"'", TempatWisata.class).getResultList();
		} catch (Exception e) {
				wisata = null;
		}
        em.close();
        return wisata;   
    }    
    
    @Override
    public List<TempatWisata> getWisataByStatusAndKategori(String kategori, String status) {
    	EntityManager em = emf.createEntityManager();
    	List<TempatWisata> wisata = null;
        try {
			wisata = em.createQuery("from TempatWisata where nama_kategori = '"+kategori+"' AND status = '"+status+"'", TempatWisata.class).getResultList();
		} catch (Exception e) {
				wisata = null;
		}
        em.close();
        return wisata;
    }
    

	@Override
    public TempatWisata saveorUpdate(TempatWisata wisata) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        wisata.setKategori(kRepo.getIdKategori(wisata.getNama_kategori()));
        TempatWisata saved = em.merge(wisata);//cek di database ada atau ngak, jika ngak create. jika ada update
        em.getTransaction().commit();
        em.close();
        return saved;
    }

    @Override
	public List<TempatWisata> getAllTambahWisataByStatus(String status) {
		EntityManager em = emf.createEntityManager();
    	List<TempatWisata> wisata = null;
        try {
			wisata = em.createQuery("from TempatWisata where status='"+status+"'", TempatWisata.class).getResultList();
		} catch (Exception e) {
				wisata = null;
		}
        em.close();
        return wisata;
	}
    
	@Override
	public void updateStatusTambahWisataById(int id_wisata) {
		TempatWisata tempatwisata = getIdWisata(id_wisata);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		tempatwisata.setTgl_konfirmasi(dtf.format(now));
		tempatwisata.setStatus("Accepted");
		tempatwisata = saveorUpdate(tempatwisata);		
	}
	
	@Override
	public void updateStatusTambahWisataIgnoreById(int id_wisata) {
		TempatWisata tempatwisata = getIdWisata(id_wisata);
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	   	 LocalDateTime now = LocalDateTime.now();
		 tempatwisata.setTgl_konfirmasi(dtf.format(now));
		tempatwisata.setStatus("Rejected");
		tempatwisata = saveorUpdate(tempatwisata);		
	}
	
	@Override
	public void updateStatusTambahWisataDisableById(int id_wisata) {
		TempatWisata tempatwisata = getIdWisata(id_wisata);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		tempatwisata.setTgl_konfirmasi(dtf.format(now));
		tempatwisata.setStatus("Disabled");
		tempatwisata = saveorUpdate(tempatwisata);		
	}	
	
	@Override
	public TempatWisata findWisataById(int id_wisata) {
		EntityManager em = emf.createEntityManager();		
		return em.find(TempatWisata.class, id_wisata);
	}
	

	
	@Override
	public List<TempatWisata> getAllTambahWisataByIdVendor(int t_akun_id) {
		EntityManager em = emf.createEntityManager();
    	List<TempatWisata> wisata = null;
        try {
			wisata = em.createQuery("from TempatWisata where t_akun_id="+t_akun_id, TempatWisata.class).getResultList();
		} catch (Exception e) {
				wisata = null;
		}
        em.close();
        return wisata;
	}
	
    @Override
    public TempatWisata getIdWisata(Integer id_wisata) {
        EntityManager em=emf.createEntityManager();
        return em.find(TempatWisata.class, id_wisata);

    }
        
    
    @Override
    public void hapus(Integer id_wisata) {
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(TempatWisata.class, id_wisata));
        em.getTransaction().commit(); 
        em.close();
    }
    
}
