package com.PSI.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.PSI.model.Role;
import com.PSI.repository.RoleRepo;

@Service
public class RoleService implements RoleRepo {
	private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;  
    }
    
    
    @Override
	public List<Role> getAllRole() {
		EntityManager em = emf.createEntityManager();
		List<Role> role= null; 
        try {
			role = em.createQuery("from Role", Role.class).getResultList();
		} catch (Exception e) {
				role = null;
		}
        em.close();
		return role;
	}
    
    @Override
    public Role getIdRole(String nama_role) {
        EntityManager em=emf.createEntityManager();
		Role role= null; 
        try {
			role = em.createQuery("from Role where nama_role='"+nama_role+"'", Role.class).getSingleResult();
		} catch (Exception e) {
				role = null;
		}
        em.close();
		return role;
    
    }
}	
