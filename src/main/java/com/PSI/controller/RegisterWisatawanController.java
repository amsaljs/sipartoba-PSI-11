package com.PSI.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.PSI.model.Akun;
//import com.PSI.model.Vendor;
import com.PSI.model.Wisatawan;
import com.PSI.service.AkunService;
import com.PSI.service.aService;
import com.PSI.service.wService;
import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;

@Controller
public class RegisterWisatawanController {
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private wService wService;
	private AkunService akunService;
	private aService aService;
	
	@Autowired
	public RegisterWisatawanController(BCryptPasswordEncoder bCryptPasswordEncoder,
			wService wService, AkunService akunService, aService aService) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.wService = wService;
		this.akunService = akunService;
		this.aService = aService;
	}
		
	
	// Return registration form template
	@RequestMapping(value="/register-wisatawan", method = RequestMethod.GET)
	public ModelAndView showRegistrationPage(Model model, ModelAndView modelAndView, Wisatawan wisatawan, HttpServletRequest request){

			modelAndView.addObject("wisatawan", wisatawan);
			modelAndView.setViewName("register-wisatawan");
			return modelAndView;


	}
	
	// Process form input data
	@RequestMapping(value = "/register-wisatawan", method = RequestMethod.POST)
	public ModelAndView processRegistrationForm(ModelAndView modelAndView, @Valid Wisatawan wisatawan, BindingResult bindingResult, 
			HttpServletRequest request, @RequestParam("username") String username, @RequestParam("password") String password,
			@RequestParam Map<String, String> requestParams, Akun akun, Model model, RedirectAttributes redir) {
		
		
		Zxcvbn passwordCheck = new Zxcvbn();
		
		Strength strength = passwordCheck.measure(requestParams.get("password"));
		
		if (strength.getScore() < 3) {
			//modelAndView.addObject("errorMessage", "Your password is too weak.  Choose a stronger one.");
			bindingResult.reject("password");
			
			redir.addFlashAttribute("errorMessage", "Password terlalu lemah.");

			modelAndView.setViewName("register-wisatawan");
			return modelAndView;
		}
		
		// Lookup user in database by e-mail
		Wisatawan wisatawanExists = wService.findByEmail(wisatawan.getEmail());
		
		System.out.println(wisatawanExists);
		
		if (wisatawanExists != null) {
			modelAndView.addObject("alreadyRegisteredMessage", "Oops!  There is already a user registered with the email provided.");
			modelAndView.setViewName("register-wisatawan");
			bindingResult.reject("email");
		}
		
		Akun exist = aService.findByUsername(requestParams.get("username"));
		System.out.println(exist);
		if (exist != null) {
			modelAndView.addObject("alreadyRegistered", "Oops!  There is already a user registered with the username provided.");
			bindingResult.reject("username");
			modelAndView.setViewName("register-wisatawan");
		}
			
		if (bindingResult.hasErrors()) { 
			modelAndView.setViewName("register-wisatawan");		
		} else { // new user so we create user and send confirmation e-mail
			
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDateTime now = LocalDateTime.now();
			wisatawan.setTgl_create(dtf.format(now));
//			 Disable user until they click on confirmation link in email


			akun.setDate_create(dtf.format(now));
			// Set new password
			akun.setNama_akun(requestParams.get("nama_wisatawan"));
			akun.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
			aService.saveAkunWisatawan(akun);
		    
			wisatawan.setAkun(akun);
		    wService.saveWisatawan(wisatawan);

		   			
			modelAndView.addObject("successMessage", "Data anda berhasil di simpan!");
			modelAndView.setViewName("register-wisatawan");
		}
			
		return modelAndView;
	}


}
