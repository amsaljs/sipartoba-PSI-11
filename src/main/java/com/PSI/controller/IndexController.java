package com.PSI.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import com.PSI.model.Akun;
import com.PSI.model.Review;
import com.PSI.model.TempatWisata;
import com.PSI.model.Vendor;
import com.PSI.repository.AkunRepo;
import com.PSI.repository.GrafikRepository;
import com.PSI.repository.KategoriRepo;
import com.PSI.repository.ReviewRepository;
import com.PSI.repository.VendorRepo;
import com.PSI.repository.RoleRepo;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.service.ReviewService;
import com.PSI.service.TempatWisataService;
import com.PSI.service.tService;
import com.PSI.service.revService;
import com.PSI.repository.vRepo;

@Controller
public class IndexController {
	
	private ReviewRepository reviewRepo;
	private RoleRepo rRepo;
	private TempatWisataRepo tRepo;
	private AkunRepo aRepo;
	private KategoriRepo kRepo;
	private VendorRepo vRepo;
	private Akun akunLogin;
	private TempatWisataService tempatWisataService;
	private ReviewService reviewService;
	private tService tempatwisataService;
	private revService revService;
	private GrafikRepository cRepo;
	private vRepo vendorRepo;
	
	@Autowired
	public void setcRepo(GrafikRepository cRepo) {
		this.cRepo = cRepo;
	}
	
	@Autowired
	public IndexController(tService tempatwisataService, revService revService) {
		this.tempatwisataService = tempatwisataService;
		this.revService = revService;
	}	
	
	public ReviewService getReviewService() {
		return reviewService;
	}
	@Autowired
	public void setReviewService(ReviewService reviewService) {
		this.reviewService = reviewService;
	}

	public tService getTempatwisataService() {
		return tempatwisataService;
	}

	public void setTempatwisataService(tService tempatwisataService) {
		this.tempatwisataService = tempatwisataService;
	}

	@Autowired
	public void setWisataService(TempatWisataService tempatWisataService) {
	   this.tempatWisataService = tempatWisataService;
	}
	
	@Autowired
	public void setuRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}
	
	@Autowired
	public void setvRepo(VendorRepo vRepo) {
		this.vRepo = vRepo;
	}

	@Autowired
	public void settRepo(TempatWisataRepo tRepo) {
		this.tRepo = tRepo;
	}
	
	@Autowired
	public void setrRepo(RoleRepo rRepo) {
		this.rRepo = rRepo;
	}
	
	@Autowired
	public void setkRepo(KategoriRepo kRepo) {
		this.kRepo = kRepo;
	}

	//================================================================ halaman awal website
	@RequestMapping(value={"/index"})
	public String indexPage(Model model) {
		model.addAttribute("data", cRepo.getAllData());
		model.addAttribute("akunLogin",new Akun());
		return "index";
	}
	
	
	@RequestMapping(value={"/home/wisatawan"}, method=RequestMethod.GET)
	public String homePageWisatawan(Model model, HttpServletRequest request) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Wisatawan")) {
			model.addAttribute("data", cRepo.getAllData());
			model.addAttribute("akunLogin",new Akun());
			return"wisatawan/homewisat";
		}
		
		return "forbidden";
	}
	
	
	// ========================== controller get halaman wisata vendor
	@RequestMapping("/wisata")
	public String wisataVendorPage(Model model, HttpServletRequest request) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Vendor")) {
			
			model.addAttribute("data", cRepo.getAllWisataVendorByIdAkun(akunLogin.getId()));
			model.addAttribute("kategori", kRepo.getAllKategori());
			model.addAttribute("allTambahWisata", tRepo.getAllTambahWisataByIdVendor(akunLogin.getId()));
			model.addAttribute("akunLogin",akunLogin);
			return "vendor/wisata";
		}
		
		return "forbidden";

	}
	
	//============================================== halaman view detail wisata vendor
	@RequestMapping(value={"/wisata/view-vendor/{id_wisata}"}, method=RequestMethod.GET)
	public String viewWisataPage(Model model, @Valid Review review, HttpServletRequest request, @PathVariable Integer id_wisata) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.removeAttribute("akunLogin");
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Vendor")) {
			TempatWisata wisata = tempatwisataService.findByIdWisata(id_wisata); 
			model.addAttribute("wisata", tempatWisataService.getWisataById(id_wisata));
			model.addAttribute("chart", cRepo.getDataByIdWisata(id_wisata));
			model.addAttribute("data", revService.findAllByNama(wisata.getNama()));
			model.addAttribute("nama", wisata.getNama());
			model.addAttribute("komentar", new Review());
			model.addAttribute("akunLogin",akunLogin);
			return "vendor/view-wisata";
		}
		
		return "forbidden";
		

	}
	
		
	// ========================================= controller post komentar
	@RequestMapping(value={"/tambah-komentar"}, method=RequestMethod.POST)
	public ModelAndView balasKomentar(ModelAndView modelAndView, Model model, @Valid Review review, HttpServletRequest request, @RequestParam Map<String, String> requestParams) {
		
		TempatWisata wisata = tempatwisataService.findByNama(requestParams.get("nama"));
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		review.setTgl_review(dtf.format(now));
		review.setNama(requestParams.get("nama"));
		review.setNama_reviewer(akunLogin.getNama_akun());
		review.setAkun(akunLogin);
		review.setWisata(wisata);
		model.addAttribute("komentar", reviewRepo.SaveOrUpdate(review));
		modelAndView.setViewName("redirect:/wisata/view-vendor/" + wisata.getId());
		return modelAndView;
	}
	
	
	// ======================================== controller add komentar dinas 
	@RequestMapping(value={"/add-komentar"}, method=RequestMethod.POST)
	public ModelAndView tambahKomentar(ModelAndView modelAndView, Model model, @Valid Review review, HttpServletRequest request, @RequestParam Map<String, String> requestParams) {
		
		TempatWisata wisata = tempatwisataService.findByNama(requestParams.get("nama"));
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		review.setTgl_review(dtf.format(now));
		review.setNama(requestParams.get("nama"));
		review.setNama_reviewer(akunLogin.getNama_akun());
		review.setAkun(akunLogin);
		review.setWisata(wisata);
		model.addAttribute("komentar", reviewRepo.SaveOrUpdate(review));
		modelAndView.setViewName("redirect:/wisata/view/" + wisata.getId());
		return modelAndView;
	}	

	// ============================================================= controller get halaman tambah wisata
	 @RequestMapping(value="/wisata/new", method = RequestMethod.GET)
	    public String tampilkanForm (Model model, TempatWisata wisata, HttpServletRequest request){
			if (request.getSession().getAttribute("akunLogin")==null) {
				request.getSession().invalidate();
				return "redirect:/index";
			}
			Akun akun = new Akun();
			akun = (Akun) request.getSession().getAttribute("akunLogin");
			model.addAttribute("akun", akun);
			if (akun.getRole_name().equals("Vendor")) {
		        model.addAttribute("wisata", new TempatWisata());
		        model.addAttribute("kategori",kRepo.getAllKategori());
		        model.addAttribute("akunLogin",akunLogin);
		        return "vendor/formWisata";
			}
			
			return "forbidden";

	 }
	 
	  @RequestMapping(value="/wisata/new", method = RequestMethod.POST)
	    public ModelAndView simpanDataWisata (ModelAndView modelAndView, BindingResult bindingResult, 
	    		@RequestParam Map<String, String> requestParams, Model model, @Valid TempatWisata wisata, 
	    		HttpServletRequest request, @RequestParam("images") MultipartFile file, Vendor vendor){
		
		  TempatWisata exist = tempatwisataService.findByNama(requestParams.get("nama"));
		  System.out.println(exist);
			if (exist != null) {
				modelAndView.addObject("alreadyRegisteredMessage", "Failed!!! Tempat Wisata sudah terdaftar");
				bindingResult.reject("nama");
				modelAndView.setViewName("vendor/formWisata");
			}
			
			if (bindingResult.hasErrors()) {
				modelAndView.addObject("alreadyRegisteredMessage", "Failed!!! Tempat Wisata sudah terdaftar");
				modelAndView.setViewName("vendor/formWisata");		
			} else {
		  if (!file.isEmpty()) {
				wisata.setGambar(file.getOriginalFilename());
				String name = file.getOriginalFilename();
				try {
					byte[] bytes = file.getBytes();
					BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(new File("D:/SIParToba/src/main/resources/static/images/gambar/"+name)));
					stream.write(bytes);
					stream.flush();
					stream.close();
					System.out.println(stream);
				} catch (Exception e) {
//					return "You failed to upload " + name + " => " + e.getMessage();
				}
			}
		  	
		    wisata.setNama_vendor(akunLogin.getNama_akun());
//		    Vendor vendors = vendorRepo.findByAkun(akunLogin.getId());
//		    wisata.setVendor(vendors);
		    wisata.setAkun(akunLogin);
			wisata.setStatus("Request");
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDateTime now = LocalDateTime.now();
			wisata.setTgl_request(dtf.format(now));
			model.addAttribute("wisata", wisata);
	        model.addAttribute("wisata",tempatWisataService.saveorUpdate(wisata));
	        model.addAttribute("akunLogin",akunLogin);
	        
	        modelAndView.addObject("successMessage", "Data wisata berhasil di simpan!");
	        modelAndView.setViewName("vendor/formWisata");
			}
	        return modelAndView;
	        
	   }
	  
	
	@RequestMapping(value ="/index", method=RequestMethod.POST)
	public ModelAndView loginProses(ModelAndView modelAndView, HttpServletRequest request, Akun akunlogin) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if(aRepo.login(username, password) != null){
			request.getSession().setAttribute("akunLogin", aRepo.login(username, password));
			akunLogin = (Akun) request.getSession().getAttribute("akunLogin");
			if (akunLogin.getRole_name().equals("Wisatawan")) {
				akunLogin = (Akun) request.getSession().getAttribute("akunLogin");
				modelAndView.setViewName("redirect:/home/wisatawan");
				return modelAndView;
			} else if (akunLogin.getRole_name().equals("Dinas Pariwisata")) {
				akunLogin = (Akun) request.getSession().getAttribute("akunLogin");
				modelAndView.setViewName("redirect:/home/dinas");
				return modelAndView;
			} else if (akunLogin.getRole_name().equals("Vendor")) {
				akunLogin = (Akun) request.getSession().getAttribute("akunLogin");
				modelAndView.setViewName("redirect:/homeVendor");
				return modelAndView;
			} 
		}
		else if(aRepo.login(username, password) == null){
			modelAndView.addObject("Message", "Username/Password tidak terdaftar");
			modelAndView.setViewName("index");
			return modelAndView;
		}

		return modelAndView;
	}
	
//=================================================================== Controller untuk wisatawan

	@RequestMapping(value={"/detail-wisata"}, method=RequestMethod.GET)
	public String detailWisataPage(Model model, @Valid Review review, HttpServletRequest request,@RequestParam("id") Integer id) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.removeAttribute("akunLogin");
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Wisatawan")) {
			TempatWisata wisata = tempatwisataService.findByIdWisata(id); 
			model.addAttribute("wisata", tempatWisataService.getWisataById(id));
			model.addAttribute("data", revService.findAllByNama(wisata.getNama()));
			model.addAttribute("nama", wisata.getNama());
			model.addAttribute("komentar", new Review());
			model.addAttribute("akunLogin",akunLogin);
			return "wisatawan/detailWisata";
		}
		
		return "forbidden";
		
	}
	
	@RequestMapping(value={"/view/detail-wisata"})
	public String viewDetailWisataPage(Model model, @Valid Review review, HttpServletRequest request,@RequestParam("id") Integer id) {
		
		TempatWisata wisata = tempatwisataService.findByIdWisata(id); 
		model.addAttribute("wisata", tempatWisataService.getWisataById(id));
		model.addAttribute("data", revService.findAllByNama(wisata.getNama()));
		model.addAttribute("nama", wisata.getNama());
		model.addAttribute("komentar", new Review());
		model.addAttribute("akunLogin",akunLogin);
		return "view-detail-wisata";
	}
	
	
	

	@RequestMapping(value={"/detail-wisata"}, method=RequestMethod.POST)
	public ModelAndView postKomentar(ModelAndView modelAndView, Model model,@Valid Review review, HttpServletRequest request, @RequestParam Map<String, String> requestParams) {

		
		TempatWisata wisata = tempatwisataService.findByNama(requestParams.get("nama"));
		modelAndView.setViewName("redirect:detail-wisata?id=" + wisata.getId());
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		review.setTgl_review(dtf.format(now));
		review.setNama(requestParams.get("nama"));
		review.setNama_reviewer(akunLogin.getNama_akun());
		review.setAkun(akunLogin);
		review.setWisata(wisata);
		model.addAttribute("komentar", reviewRepo.SaveOrUpdate(review));
		return modelAndView;
	}


	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request){
		request.getSession().invalidate();
		return "redirect:/index";
	}

	public AkunRepo getaRepo() {
		return aRepo;
	}

	public void setaRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}

	public Akun getAkunLogin() {
		return akunLogin;
	}

	public void setAkunLogin(Akun akunLogin) {
		this.akunLogin = akunLogin;
	}

	public RoleRepo getrRepo() {
		return rRepo;
	}

	public TempatWisataRepo gettRepo() {
		return tRepo;
	}

	public TempatWisataService getTempatWisataService() {
		return tempatWisataService;
	}

	public void setTempatWisataService(TempatWisataService tempatWisataService) {
		this.tempatWisataService = tempatWisataService;
	}

	public KategoriRepo getkRepo() {
		return kRepo;
	}

	public VendorRepo getvRepo() {
		return vRepo;
	}
	
	public ReviewRepository getReviewRepo() {
		return reviewRepo;
	}
	@Autowired
	public void setReviewRepo(ReviewRepository reviewRepo) {
		this.reviewRepo = reviewRepo;
	}	
		
}
