package com.PSI.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.PSI.model.TempatWisata;
import com.PSI.model.Akun;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.repository.GrafikRepository;
import com.PSI.repository.ReviewRepository;

@Controller
public class WisatawanController {
	private ReviewRepository reviewRepo;
	private Akun akunLogin;
	private TempatWisata wisatas;
	private GrafikRepository cRepo;
	
	@Autowired
	public void setcRepo(GrafikRepository cRepo) {
		this.cRepo = cRepo;
	}
	
	public TempatWisata getWisata() {
		return wisatas;
	}
	public void setWisata(TempatWisata wisatas) {
		this.wisatas = wisatas;
	}
	public Akun getAkunLogin() {
		return akunLogin;
	}
	
	public void setAkunLogin(Akun akunLogin) {
		this.akunLogin = akunLogin;
	}
	public ReviewRepository getReviewRepo() {
		return reviewRepo;
	}
	@Autowired
	public void setReviewRepo(ReviewRepository reviewRepo) {
		this.reviewRepo = reviewRepo;
	}

	private TempatWisataRepo wisataRepo;

	/**
	 * @return the wisataRepo
	 */
	public TempatWisataRepo getWisataRepo() {
		return wisataRepo;
	}

	/**
	 * @param wisataRepo the wisataRepo to set
	 */
	@Autowired
	public void setWisataRepo(TempatWisataRepo wisataRepo) {
		this.wisataRepo = wisataRepo;
	}

	
	//====================================== get halaman wisatawan/allWisata
	@RequestMapping(value={"/wisata/all"}, method=RequestMethod.GET)
	public String WisataPage(Model model, TempatWisata wisata, HttpServletRequest request) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.removeAttribute("akunLogin");
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Wisatawan")) {
			model.addAttribute("wisata", wisataRepo.listWisata());
			return "wisatawan/wisata-all";
		}
		
		return "forbidden";

	}
	
	
	@RequestMapping(value={"/view/daftar/wisata"})
	public String viewWisataPage(Model model) {
		model.addAttribute("wisata", wisataRepo.listWisata());
		return "view-wisata";
	}
	
	
	
	//======================================= get halaman hot wisata
	@RequestMapping(value={"/hot/wisata"}, method=RequestMethod.GET)
	public String hotWisataPage(Model model, TempatWisata wisata, HttpServletRequest request) {
//		if (request.getSession().getAttribute("akunLogin")==null) {
//			request.removeAttribute("akunLogin");
//			request.getSession().invalidate();
//			return "redirect:/index";
//		}
		model.addAttribute("wisata", cRepo.getAllDataHotWisata());
		return "wisatawan/hotWisata";
	}



}
