 package com.PSI.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


import com.PSI.model.Akun;
//import com.PSI.model.TempatWisata;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.repository.KategoriRepo;
import com.PSI.repository.RoleRepo;
import com.PSI.repository.AkunRepo;
import com.PSI.service.TempatWisataService;


@Controller
public class TempatWisataController {
    private TempatWisataService tempatWisataService;
    
	private RoleRepo rRepo;
	private TempatWisataRepo tRepo;
	private AkunRepo aRepo;
	private KategoriRepo kRepo;
	private Akun akunLogin;

	
	@Autowired
	public void setuRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}
	
	@Autowired
	public void settRepo(TempatWisataRepo tRepo) {
		this.tRepo = tRepo;
	}
	
	@Autowired
	public void setrRepo(RoleRepo rRepo) {
		this.rRepo = rRepo;
	}
	
	@Autowired
	public void setkRepo(KategoriRepo kRepo) {
		this.kRepo = kRepo;
	}

    @Autowired
    public void setWisataService(TempatWisataService tempatWisataService) {
        this.tempatWisataService = tempatWisataService;
    }
    
 
	
   @RequestMapping(value="/wisata/hapus/{id_wisata}")
   public String hapusWisata(@PathVariable Integer id_wisata) {
	   tempatWisataService.hapus(id_wisata);
	   return "redirect:/wisata";
   }
   


	public TempatWisataService getTempatWisataService() {
		return tempatWisataService;
	}
	
	public void setTempatWisataService(TempatWisataService tempatWisataService) {
		this.tempatWisataService = tempatWisataService;
	}
	
	public AkunRepo getaRepo() {
		return aRepo;
	}
	
	public void setaRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}
	
	public Akun getAkunLogin() {
		return akunLogin;
	}
	
	public void setAkunLogin(Akun akunLogin) {
		this.akunLogin = akunLogin;
	}
	
	public RoleRepo getrRepo() {
		return rRepo;
	}
	
	public TempatWisataRepo gettRepo() {
		return tRepo;
	}
	
	public KategoriRepo getkRepo() {
		return kRepo;
	}
	
  }
 