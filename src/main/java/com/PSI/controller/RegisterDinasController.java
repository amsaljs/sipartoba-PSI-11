package com.PSI.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//import com.PSI.model.User;
import com.PSI.model.Akun;
import com.PSI.model.Dinas;


import com.PSI.service.AkunService;
import com.PSI.service.dService;
import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;

@Controller
public class RegisterDinasController {
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private dService dService;
	private AkunService akunService;
	
	
	@Autowired
	public RegisterDinasController(BCryptPasswordEncoder bCryptPasswordEncoder,
			dService dService,  AkunService akunService) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.dService = dService;
		this.akunService = akunService;
		
	}
	
	@RequestMapping(value="/register-dinas", method = RequestMethod.GET)
	public ModelAndView showRegistrationPage(ModelAndView modelAndView, Dinas dinas){
		modelAndView.addObject("dinas", dinas);
		modelAndView.setViewName("register-dinas");
		return modelAndView;
	}
	
	// Process form input data
	@RequestMapping(value ="/register-dinas", method = RequestMethod.POST)
	public ModelAndView processRegistrationForm(ModelAndView modelAndView, @Valid Dinas dinas, BindingResult bindingResult, HttpServletRequest request) {
				
		// Lookup user in database by e-mail
		Dinas dinasExists = dService.findByEmail(dinas.getEmail());
		
		System.out.println(dinasExists);
		
		if (dinasExists != null) {
			modelAndView.addObject("alreadyRegisteredMessage", "Oops!  There is already a user registered with the email provided.");
			modelAndView.setViewName("register-dinas");
			bindingResult.reject("email");
		}
			
		if (bindingResult.hasErrors()) { 
			modelAndView.setViewName("register-dinas");		
		} else { // new user so we create user and send confirmation e-mail
					
			// Disable user until they click on confirmation link in email
		    dinas.setEnable(false);
		      
		    // Generate random 36-character string token for confirmation link
		    dinas.setConfirmationToken(UUID.randomUUID().toString());
		    //set default name dinas pariwisata
		    dinas.setNama_dinas("Dinas Pariwisata Tobasa");
		    dService.saveDinas(dinas);
				
			String appUrl = request.getScheme() + "://" + request.getServerName();
			
			
//			modelAndView.addObject("confirmationMessage", "Link konfirmasi telah dikirim ke " + vendor.getEmail());
			modelAndView.addObject("confirmationMessage","To confirm your e-mail address, please click the link below:\n"
					+ appUrl + ":8080/confirm-dinas?token=" + dinas.getConfirmationToken());
			modelAndView.setViewName("register-dinas");
		}
			
		return modelAndView;
	}


	// Return registration form template

	// Process confirmation link
	@RequestMapping(value="/confirm-dinas", method = RequestMethod.GET)
	public ModelAndView confirmRegistration(ModelAndView modelAndView, @RequestParam("token") String token, Akun akun) {
			
		Dinas dinas = dService.findByConfirmationToken(token);
			
		if (dinas == null) { // No token found in DB
			modelAndView.addObject("invalidToken", "Oops!  Invalid Link.");
		} else { // Token found
			modelAndView.addObject("confirmationToken", dinas.getConfirmationToken());
		}
		
		modelAndView.setViewName("confirm-dinas");
		return modelAndView;		
	}
	
	// Process confirmation link
	@RequestMapping(value="/confirm-dinas", method = RequestMethod.POST)
	public ModelAndView confirmRegistration(HttpServletRequest request, Model model, ModelAndView modelAndView, BindingResult bindingResult, @RequestParam Map<String, String> requestParams, RedirectAttributes redir,@Valid Akun akun) {
		
		modelAndView.setViewName("confirm-dinas");
		
		Zxcvbn passwordCheck = new Zxcvbn();
		
		Strength strength = passwordCheck.measure(requestParams.get("password"));
		
		if (strength.getScore() < 3) {
			//modelAndView.addObject("errorMessage", "Your password is too weak.  Choose a stronger one.");
			bindingResult.reject("password");
			
			redir.addFlashAttribute("errorMessage", "Password terlalu lemah.");

			modelAndView.setViewName("redirect:confirm-dinas?token=" + requestParams.get("token"));
			System.out.println(requestParams.get("token"));
			return modelAndView;
		}
	
		// Find the user associated with the reset token
		Dinas dinas = dService.findByConfirmationToken(requestParams.get("token"));

		// Set new password
		akun.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
		
		//set date created akun
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		akun.setDate_create(dtf.format(now));
		
		// Set user to enabled
		dinas.setEnable(true);
		
		//set id dinas dalam tabel akun
//		akun.setDinas(dinas);
		akun.setNama_akun(dinas.getNama_dinas());
		// Save akun
		model.addAttribute("akun", akunService.saveOrUpdateDinas(akun));

		//save vendor
	    dService.saveDinas(dinas);
		
		modelAndView.addObject("successMessage", "Data anda berhasil di simpan!");
		return modelAndView;		
	}
	
}