package com.PSI.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//import com.PSI.model.User;
import com.PSI.model.Akun;
import com.PSI.model.Vendor;
import com.PSI.service.aService;
import com.PSI.service.AkunService;
import com.PSI.service.vService;
import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;

@Controller
public class RegisterVendorController {
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private vService vService;
	
	private AkunService akunService;
	private aService aService;
	
	@Autowired
	public RegisterVendorController(BCryptPasswordEncoder bCryptPasswordEncoder,
			vService vService,  AkunService akunService, aService aService) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.vService = vService;
		this.akunService = akunService;
		this.aService = aService;
	}
	
	// Return registration form template
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public ModelAndView showRegistrationPage(ModelAndView modelAndView, Vendor vendor){
		modelAndView.addObject("vendor", vendor);
		modelAndView.setViewName("register");
		return modelAndView;
	}
	
	// Process form input data
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView processRegistrationForm(ModelAndView modelAndView, @Valid Vendor vendor, BindingResult bindingResult, 
			HttpServletRequest request, @RequestParam Map<String, String> requestParams, 
			RedirectAttributes redir, Akun akun, @RequestParam("username") String username, 
			@RequestParam("password") String password, Model model ) {
		
		Zxcvbn passwordCheck = new Zxcvbn();
		
		Strength strength = passwordCheck.measure(requestParams.get("password"));
		
		if (strength.getScore() < 3) {
			//modelAndView.addObject("errorMessage", "Your password is too weak.  Choose a stronger one.");
			bindingResult.reject("password");
			
			redir.addFlashAttribute("errorMessage", "Password terlalu lemah.");

			modelAndView.setViewName("register");
			return modelAndView;
		}		
		// Lookup user in database by e-mail
		Vendor vendorExists = vService.findByEmail(vendor.getEmail());
		System.out.println(vendorExists);
		
		if (vendorExists != null) {
			modelAndView.addObject("alreadyRegisteredMessage", "Oops!  There is already a user registered with the email provided.");
			modelAndView.setViewName("register");
			bindingResult.reject("email");
		}
		
		Akun exist = aService.findByUsername(requestParams.get("username"));
		System.out.println(exist);

		if (exist != null) {
			modelAndView.addObject("alreadyRegistered", "Oops!  There is already a user registered with the username provided.");
			bindingResult.reject("username");
			modelAndView.setViewName("register");
		}
		
			
		if (bindingResult.hasErrors()) { 
			modelAndView.setViewName("register");		
		} else { // new user so we create user and send confirmation e-mail
			
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDateTime now = LocalDateTime.now();
			akun.setDate_create(dtf.format(now));
			
			akun.setNama_akun(requestParams.get("nama_vendor"));
			akun.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
			
			aService.saveAkun(akun);
			
			vendor.setAkun(akun);
		    vService.saveVendor(vendor);
				
		    modelAndView.addObject("successMessage", "Data anda berhasil di simpan!");
			modelAndView.setViewName("register");
		}
			
		return modelAndView;
	}

}