package com.PSI.controller;

//import java.util.List;
import java.util.Map;



import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.PSI.model.Akun;
import com.PSI.model.Fasilitas;
import com.PSI.model.TempatWisata;
import com.PSI.repository.FasilitasRepo;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.service.tService;
import com.PSI.service.FasiltasService;


@Controller
public class VendorController {
	private TempatWisataRepo tRepo;
	private FasilitasRepo fRepo;
	private tService tempatwisataService;
	@Autowired
	private FasiltasService fService;
	
	@Autowired
	public VendorController(tService tempatwisataService) {
		this.tempatwisataService = tempatwisataService;
	}	
	
	public FasilitasRepo getfRepo() {
		return fRepo;
	}
	@Autowired
	public void setfRepo(FasilitasRepo fRepo) {
		this.fRepo = fRepo;
	}
	@Autowired
	public void settRepo(TempatWisataRepo tRepo) {
		this.tRepo = tRepo;
	}
	@RequestMapping(value={"/homeVendor"}, method=RequestMethod.GET)
	public String vendorPage(HttpServletRequest request, Model model) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Vendor")) {
			return "vendor/homeVendor";
		}
		
		return "forbidden";
	}
	
	//fasilitas view page
	@RequestMapping(value={"/view/fasilitas"}, method=RequestMethod.GET)
	public String FormFasilitasView(Model model ,HttpServletRequest request, @RequestParam("id") Integer id) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Vendor")) {
			
			model.addAttribute("fasilitas", fRepo.getAllFasilitasByIdWisata(id));
			model.addAttribute("wisata", tRepo.getWisataById(id));
			return "vendor/fasilitas-view";
		}
		
		return "forbidden";

	}
	
	//fasilitas-form page
	@RequestMapping(value={"/fasilitas"}, method=RequestMethod.GET)
	public ModelAndView FormFasilitas(ModelAndView modelAndView,Model model ,HttpServletRequest request, @RequestParam("id") Integer id) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			modelAndView.setViewName("/index");
			return modelAndView;
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Vendor")) {
			
			TempatWisata wisata = tempatwisataService.findByIdWisata(id);
			modelAndView.addObject("namaWisata", wisata.getNama());
			model.addAttribute("wisata", tRepo.getWisataById(id));
			model.addAttribute("fasilitas", new Fasilitas());
			modelAndView.setViewName("vendor/form-fasilitas");
			return modelAndView;
		}
		modelAndView.setViewName("forbidden");
		return modelAndView;
	}

	//save data fasilitas
	@RequestMapping(value={"/fasilitas"}, method=RequestMethod.POST)
	public ModelAndView SimpanFasilitas(@RequestParam Map<String, String> requestParams, Model model ,HttpServletRequest request, Fasilitas fasilitas, ModelAndView modelAndView) {
		
		modelAndView.setViewName("vendor/form-fasilitas");
		
		TempatWisata wisata = tempatwisataService.findByNama(requestParams.get("nama"));
		fasilitas.setWisata(wisata);
		
		model.addAttribute("fasilitas", fService.SaveOrUpdate(fasilitas));
		
		modelAndView.addObject("successMessage", "Data berhasil di simpan!");
		return modelAndView;
	}
	
	public TempatWisataRepo gettRepo() {
		return tRepo;
	}
	
	
}