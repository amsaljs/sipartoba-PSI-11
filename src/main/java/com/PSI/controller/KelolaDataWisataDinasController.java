package com.PSI.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.PSI.model.Akun;
import com.PSI.repository.AkunRepo;
import com.PSI.repository.GrafikRepository;
import com.PSI.repository.KategoriRepo;
import com.PSI.repository.RoleRepo;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.service.TempatWisataService;

@Controller
public class KelolaDataWisataDinasController {
	
	private TempatWisataService tempatWisataService;
	private RoleRepo rRepo;
	private TempatWisataRepo tRepo;
	private AkunRepo aRepo;
	private KategoriRepo kRepo;
	private Akun akunLogin;
	private GrafikRepository cRepo;
	
	
	@Autowired
	public void setcRepo(GrafikRepository cRepo) {
		this.cRepo = cRepo;
	}

	@Autowired
	public void setuRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}
	
	@Autowired
	public void settRepo(TempatWisataRepo tRepo) {
		this.tRepo = tRepo;
	}
	
	@Autowired
	public void setrRepo(RoleRepo rRepo) {
		this.rRepo = rRepo;
	}
	
	@Autowired
	public void setkRepo(KategoriRepo kRepo) {
		this.kRepo = kRepo;
	}

    @Autowired
    public void setWisataService(TempatWisataService tempatWisataService) {
        this.tempatWisataService = tempatWisataService;
    }

	public TempatWisataService getTempatWisataService() {
		return tempatWisataService;
	}

	public void setTempatWisataService(TempatWisataService tempatWisataService) {
		this.tempatWisataService = tempatWisataService;
	}
	


	  @RequestMapping("/request/wisata")
	  public String WisataRequestPage(Model model, HttpServletRequest request)  {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Dinas Pariwisata")) {
			  model.addAttribute("wisata", tempatWisataService.listWisata());
			  model.addAttribute("lineChart", cRepo.listWisata());
			  model.addAttribute("pieChart", cRepo.listGrafik());
		      return "dinas/wisataDinas";
		}
		
		return "forbidden";

	  }


	public AkunRepo getaRepo() {
		return aRepo;
	}

	public void setaRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}

	public Akun getAkunLogin() {
		return akunLogin;
	}

	public void setAkunLogin(Akun akunLogin) {
		this.akunLogin = akunLogin;
	}

	public RoleRepo getrRepo() {
		return rRepo;
	}

	public TempatWisataRepo gettRepo() {
		return tRepo;
	}

	public KategoriRepo getkRepo() {
		return kRepo;
	}

}
