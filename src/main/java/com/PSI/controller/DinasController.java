package com.PSI.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;

import com.PSI.model.Akun;
import com.PSI.model.Review;
import com.PSI.model.TempatWisata;
import com.PSI.repository.AkunRepo;
import com.PSI.repository.GrafikRepository;
import com.PSI.repository.KategoriRepo;
import com.PSI.repository.RoleRepo;
import com.PSI.repository.TempatWisataRepo;
import com.PSI.repository.WisatawanRepo;
import com.PSI.service.ReviewService;
import com.PSI.service.TempatWisataService;
import com.PSI.service.revService;
import com.PSI.service.tService;

@Controller
public class DinasController {
private TempatWisataService tempatWisataService;
    
	private RoleRepo rRepo;
	private TempatWisataRepo tRepo;
	private AkunRepo aRepo;
	private KategoriRepo kRepo;
	private Akun akunLogin;
	private GrafikRepository cRepo;
	private revService revService;
	private ReviewService reviewService;
	private tService tempatwisataService;
	private WisatawanRepo wisatawanRepo;
	
	
	
	
	public GrafikRepository getcRepo() {
		return cRepo;
	}
	public WisatawanRepo getWisatawanRepo() {
		return wisatawanRepo;
	}
	@Autowired
	public void setWisatawanRepo(WisatawanRepo wisatawanRepo) {
		this.wisatawanRepo = wisatawanRepo;
	}
	public ReviewService getReviewService() {
		return reviewService;
	}
	@Autowired
	public void setReviewService(ReviewService reviewService) {
		this.reviewService = reviewService;
	}
	public tService getTempatwisataService() {
		return tempatwisataService;
	}
	@Autowired
	public void setTempatwisataService(tService tempatwisataService) {
		this.tempatwisataService = tempatwisataService;
	}
	public revService getRevService() {
		return revService;
	}
	@Autowired
	public void setRevService(revService revService) {
		this.revService = revService;
	}

	@Autowired
	public void setcRepo(GrafikRepository cRepo) {
		this.cRepo = cRepo;
	}
	
	@Autowired
	public void setuRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}
	
	@Autowired
	public void settRepo(TempatWisataRepo tRepo) {
		this.tRepo = tRepo;
	}
	
	@Autowired
	public void setrRepo(RoleRepo rRepo) {
		this.rRepo = rRepo;
	}
	
	@Autowired
	public void setkRepo(KategoriRepo kRepo) {
		this.kRepo = kRepo;
	}

    @Autowired
    public void setWisataService(TempatWisataService tempatWisataService) {
        this.tempatWisataService = tempatWisataService;
    }

	public TempatWisataService getTempatWisataService() {
		return tempatWisataService;
	}

	public void setTempatWisataService(TempatWisataService tempatWisataService) {
		this.tempatWisataService = tempatWisataService;
	}
	
	
	@RequestMapping(value={"/home/dinas"}, method=RequestMethod.GET)
	public String homePageWisatawan(HttpServletRequest request, Model model) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.removeAttribute("akunLogin");
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Dinas Pariwisata")) {
			model.addAttribute("wisatawan", wisatawanRepo.getAllWisatawan());
			model.addAttribute("chart", cRepo.getAllDataWisatawan());
			model.addAttribute("pie", cRepo.getAllDataJenis());
			model.addAttribute("data", cRepo.listGrafik());
			return "dinas/homeDinas";
		}
		
		return "forbidden";

	}
	
  
  @RequestMapping(value = "/updateWisata/{id_wisata}")
	public String updateTambahwisata(@PathVariable int id_wisata, TempatWisata wisata, Model model, HttpServletRequest request) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Dinas Pariwisata")) {
			tRepo.updateStatusTambahWisataById(id_wisata);
			return "redirect:/request/wisata";
		}
		
		return "forbidden";

	}
  @RequestMapping(value = "/updateWisataIgnore/{id_wisata}")
	public String updateTambahwisataIgnore(@PathVariable int id_wisata, TempatWisata wisata, Model model,HttpServletRequest request) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			request.removeAttribute("akunLogin");
			return "redirect:/";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Wisatawan")) {
			tRepo.updateStatusTambahWisataIgnoreById(id_wisata);
			return "redirect:/request/wisata";
		}
		
		return "forbidden";

	}
  
  @RequestMapping(value = "/updateWisataDisable/{id_wisata}")
	public String updateTambahwisataDisable(@PathVariable int id_wisata, TempatWisata wisata, Model model,HttpServletRequest request) {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			request.removeAttribute("akunLogin");
			return "redirect:/";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Dinas Pariwisata")) {
			tRepo.updateStatusTambahWisataDisableById(id_wisata);
			return "redirect:/request/wisata";
		}
		
		return "forbidden";

	}
  
	//======================================== controller wisata detail wisata dinas
  @RequestMapping("/wisata/view/{id_wisata}")
  public String WisataViewPageAdmin(Model model,@PathVariable Integer id_wisata,HttpServletRequest request)  {
		if (request.getSession().getAttribute("akunLogin")==null) {
			request.getSession().invalidate();
			return "redirect:/index";
		}
		Akun akun = new Akun();
		akun = (Akun) request.getSession().getAttribute("akunLogin");
		model.addAttribute("akun", akun);
		if (akun.getRole_name().equals("Dinas Pariwisata")) {
			  TempatWisata wisata = tempatwisataService.findByIdWisata(id_wisata); 
			  model.addAttribute("data", cRepo.listGrafik());	
			  model.addAttribute("nama", wisata.getNama());
			  model.addAttribute("wisata",tempatWisataService.getWisataById(id_wisata));
			  model.addAttribute("komentar", new Review());
			  model.addAttribute("datas", revService.findAllByNama(wisata.getNama()));
		      return "dinas/viewWisata";
		}
		
		return "forbidden";

  }	
  
  @RequestMapping(value="/hapus/komentar/{id_review}")
  public String hapusWisata(@PathVariable Integer id_review) {
	   reviewService.hapus(id_review);
	   return "redirect:/request/wisata";
  }

	public AkunRepo getaRepo() {
		return aRepo;
	}

	public void setaRepo(AkunRepo aRepo) {
		this.aRepo = aRepo;
	}

	public Akun getAkunLogin() {
		return akunLogin;
	}

	public void setAkunLogin(Akun akunLogin) {
		this.akunLogin = akunLogin;
	}

	public RoleRepo getrRepo() {
		return rRepo;
	}

	public TempatWisataRepo gettRepo() {
		return tRepo;
	}

	public KategoriRepo getkRepo() {
		return kRepo;
	}
    
    
}
